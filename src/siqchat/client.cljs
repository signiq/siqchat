(ns siqchat.client
  (:require
    [reagent.core :as reagent :refer [atom]]
    [chord.client :refer [ws-ch]]
    [cljs.core.async :as a]))

(enable-console-print!)

(println "This text is printed from src/siqchat/core.cljs. Go ahead and edit it and see reloading in action.")

;; define your app data so that it doesn't get over-written on reload

(defonce app-state (atom {:user "alexh"
                          :messages []}))


(defn try-connect!
  []
  (ws-ch "ws://localhost:8111/ws/" {:format :json-kw}))


(defn initialise-ws!
  []
  (a/go
    (let [{:keys [ws-channel error] :as result} (a/<! (try-connect!))])))


(initialise-ws!)

(defn enter-handler
  [ev]
  (if (= (.-key ev) "Enter")
    (let [content (.-value (.-target ev))
          msg {:content content
               :user "alexh"}]
      (a/put! (:ws-ch @app-state) msg))))


(defn hello-world []
  [:div
   [:h1 "SiqChat"]])


(reagent/render-component [hello-world]
                          (. js/document (getElementById "app")))

(defn on-js-reload [])
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)

