(ns siqchat.server
  (:require
    [chord.http-kit :as chord]
    [org.httpkit.server :refer [run-server]]
    [compojure.core :refer [GET POST defroutes]]
    [compojure.route :refer [not-found resources]]
    [clojure.core.async :as a]
    [ring.middleware.defaults :refer [api-defaults wrap-defaults]]
    [prone.middleware :refer [wrap-exceptions]]
    [ring.middleware.reload :refer [wrap-reload]]
    [medley.core :refer [random-uuid]])
  (:import (java.util Date)))


(defonce message-history (atom []))
(defonce message-ch (a/chan))
(defonce message-mult (a/mult message-ch))


(defn validate-message
  [msg]
  (when (map? msg)
    (assoc (:message msg)
      :timestamp (new Date)
      :message-id (random-uuid))))


(defn accept-chat-client
  "Take a message pub and a request, and sub the request's websocket to
  the pub on that device's topic."
  [req]
  (chord/with-channel req ws-channel {:format :json-kw}

    ;Send any messages from the client up to our message stream
    ;(a/pipe ws-channel message-ch false)
    (a/go
      (loop []
        (let [msg (a/<! ws-channel)]
          (when-not (nil? msg)
            (if-let [validated-msg (validate-message msg)]
              (a/>! message-ch validated-msg)
              (println (format "invalid message received: %s" msg)))
            (recur))))
      (println "the client disconnected"))

    ;Copy message history onto the newly-connected websocket channel
    ;(a/onto-chan ws-channel @message-history false)
    (a/go-loop [msgs (seq @message-history)]
      (when (some? msgs)
        (a/>! ws-channel (first msgs))
        (recur (next msgs))))

    ;Send all messages from the stream down to the websocket client
    (a/tap message-mult ws-channel)))


(defn save-history!
  [message-mult]
  (let [save-ch (a/chan)]
    (a/tap message-mult save-ch)
    (a/go
      (loop []
        (swap! message-history conj (a/<! save-ch))
        (recur)))))


;; route handling stuff below
(defroutes routes
  ; Handle websocket requests from the client
  (GET "/ws/" req (accept-chat-client req))
  ; Frontend
  ;(GET "/chat/" [] loading-page)
  (resources "/")
  ; 404
  (not-found "Not Found"))


(defn wrap-middleware [handler]
  (-> handler
      (wrap-defaults api-defaults)
      wrap-exceptions
      wrap-reload))


(def app (wrap-middleware #'routes))


(defn -main
  [& args]
  (run-server app {:port 8111})
  (save-history! message-mult))
